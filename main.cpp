/*==========================================================================================================
RA 1645200
RA 1643157
============================================================================================================*/

/*! \file main.cpp
    \brief Main source file containing all functions, allowing this program to work.
    
    Unique source file.
*/

/*! \mainpage Command line program
 *
 * \section intro_sec Getting started
 * 
 * Get git ready to use from command line then type:
 * git clone https://bitbucket.org/Murillo2380/project-os-2016-1-command-line-program.git
 * P.S: This is a public repository.
 *
 * \subsection step1 Step 1: Usage
 * Start the cmd by pressing WIN+R and entering cmd into the box, then press enter.
 * After doing this, navigate to the folder containing the main.cpp source code than compile it with G++ (tested only with G++ 4.9.3).
 * Example of compilation, type <b>g++ -o maincppexecutablefile main.cpp</b>
 * Execute the generated .exe file from the command line, here some examples:
 *
 * Typing the following prints information about your cpu:
 * maincppexecutablefile cpu
 * 
 * Typing the following prints the Address of Entry Point of some .exe file in the given path:
 * maincppexecutablefile ep path/to/somefile.exe
 *
 * Type maincppexecutablefile help for available parameters.
 
 */

#include <iostream> // cout
#include <iomanip> // for hex output
#include <fstream> // for file handling
#include <cstring>
#include <cassert>

/*! Enable functions and structs introduced in Windows XP or greater (MEMORYSTATUSEX for example). 
 Refers to: https://msdn.microsoft.com/pt-br/library/windows/desktop/aa383745(v=vs.85).aspx 
 */
#define WINVER 0x0501

#ifdef _WIN32
    #include <windows.h>
	
#else
    #error "System not supported."

#endif

/**
 * Used to convert bytes to MB
*/
const int DIV_MB = 1048576;
/**
 * Used to print if the .exe file is corrupted.
*/
const char EOF_MESSAGE[] = "EOF Reached, is the exe file corrupted?";

/**
 * Prints a help message, listing the available app arguments from command line.
 */
void printHelp();

/**
 * Prints CPU information like cpu type and number of cores.
 */
void printCpu();

/** 
 * Prints the current OS version.
 */
void printVersion();

/**
 * Prints RAM information (MB available, used and free).
 */
void printRam();

/** 
 * Prints the size of page file in MB, for this system.
 */
void printSwap();

/**
 * Prints the Entry Point address from the given exe file.
 * @param path Null terminated string containing the path to file. Cannot be null.
 */
void printEntryPoint(char *path);

/**
 * Check if this machine is little or big endian.
 * @return 1 if little endian, 0 otherwise.
 */
int isLittleEndian();

/**
 * Prints all files located in the given dir path. If the file is a dir, <DIR> tag will be added to its name on output.
 * @param path Path null terminating string containing the path to a directory. Cannot be null.
 */
void printDir(char *path);

/**
 * Prints environment variables for this process.
 */
 
void printPath();

/**
 * First function to be called when the program first start.
 * @param argc Number of arguments gave on command line (cmd).
 * @param argv Array of strings passed in command line (cmd).
 * @return 0 if the program succeeds on execution, another number gave by the system otherwise.
 */
int main(int argc, char *argv[]){
	
    if(argc == 1){
		
        printHelp();
        return 0;
		
    }
	
    if(!strcmp(argv[1], "help")){
		
        printHelp();
        return 0;
		
    }
 
    if(!strcmp(argv[1],"cpu")){

        printCpu();
        return 0;
		
    }

    if(!strcmp(argv[1],"version")){

        printVersion();
        return 0;	
		
    }

    if(!strcmp(argv[1], "ep")){
		
        if(argc < 3){
            std::cout << "Missing path...";
            return 0;
        }
		
        printEntryPoint(argv[2]);
        return 0;
    }

    if(!strcmp(argv[1], "ram")){
		
        printRam();
        return 0;
		
    }

    if(!strcmp(argv[1], "swap")){
		
        printSwap();
        return 0;
		
    }
	
    if(!strcmp(argv[1], "dir")){
		
        if(argc < 3){
            std::cout << "Missing dir path...";
            return 0;
        }
		
        printDir(argv[2]);
        return 0;
		
    }
	
    if(!strcmp(argv[1], "path")){
		
        printPath();
        return 0;
		
    }
	
    std::cout << "Not a valid command..\n";

    return 0;
}

void printHelp(){
	
    std::cout << "\nHelp information: type one of the following as a program parameter:\n\n"
	"cpu -- Shows information about the CPU\n"
	"version -- Current SO version (deprecated)\n"
	"ram -- Physical memory information (Total/in use)\n"
	"ep <path> -- Prints the entry point for the given .exe program, on \"path\" location.\n"
	"swap -- Shows the size of system page file.\n"
	"path -- Prints environment variables for this process.\n"
	"dir <path> -- Prints all files in alphabetic order in the given \"path\" (without quotes)\n\n";	

}

void printSwap(){
	
    MEMORYSTATUSEX statex;

    statex.dwLength = sizeof (statex); // doc says this is needed before call GlobalMemoryStatusEx.
    GlobalMemoryStatusEx (&statex);
	
    std::cout << "There are " << statex.ullTotalPageFile/DIV_MB << " MBytes of page file.\n";
	
}

void printRam(){
	
    MEMORYSTATUSEX statex;

    statex.dwLength = sizeof (statex); // doc says this is needed before call GlobalMemoryStatusEx.
    GlobalMemoryStatusEx (&statex);

    std::cout << "There are " << statex.dwMemoryLoad << " percent of memory in use\n";
    std::cout << "There are " << statex.ullTotalPhys/DIV_MB << " total MBytes of physical memory\n ";
    std::cout << "There are " << statex.ullAvailPhys/DIV_MB << " free MBytes of physical memory\n";
    std::cout << "There are " << statex.ullTotalPhys/DIV_MB-statex.ullAvailPhys/DIV_MB << " MBytes of physical memory in use\n";
		
}

void printVersion(){
	
    DWORD dwVersion = 0; 
    DWORD dwMajorVersion = 0;
    DWORD dwMinorVersion = 0; 
    DWORD dwBuild = 0;

    dwVersion = GetVersion();

    // Get the Windows version.

    dwMajorVersion = (DWORD)(LOBYTE(LOWORD(dwVersion)));
    dwMinorVersion = (DWORD)(HIBYTE(LOWORD(dwVersion)));

    // Get the build number.

    if (dwVersion < 0x80000000){              
        dwBuild = (DWORD)(HIWORD(dwVersion));
	}
	
    std::cout << "Version is " << dwMajorVersion << "." << dwMinorVersion << " (" << dwBuild << ")\n";
	
}

void printCpu(){
	
    SYSTEM_INFO siSysInfo;
    GetSystemInfo(&siSysInfo); // Copia a informação do HARDWARE para a estrutura SYSTEM_INFO. 
	
    std::cout << "CPU type: " << siSysInfo.dwProcessorType << "\n"; 
    std::cout << "Cores: " << siSysInfo.dwNumberOfProcessors << "\n"; 
	
}

void printEntryPoint(char *path){ // refers to http://www.tutorialspoint.com/cplusplus/cpp_files_streams.htm
	
    assert(path != NULL);
	
    std::ifstream file(path, std::ios::in);

    int isLE = isLittleEndian();
	
    unsigned int i = 0;
    unsigned long offsetFromPE = 6l * sizeof(char) + 5l * sizeof(short) + 6l * sizeof(long);
	
    unsigned char peHeaderOffset[4];
    unsigned char longSize = sizeof(long);
    unsigned char addressOfEntryPoint[longSize];
	
	
    if(!file){
        std::cout << "File not found or cannot open it\n";
        return;
    }
	
    file.seekg(0x3C);// seek from the beginning of the file to offset 60, where we have a pointer to PE header.
	
    if(file.eof()){
        std::cout << EOF_MESSAGE;
        return;
    }
	
    while(i < longSize){
	
        if(file.eof()){
            std::cout << EOF_MESSAGE;
            return;
        }

        if(isLE){
            file >> peHeaderOffset[i++];
            continue;
        }

        file >> peHeaderOffset[longSize - 1 - i++];
	
    }
	
    offsetFromPE += * ((unsigned long *) peHeaderOffset);
    file.seekg(offsetFromPE, std::ios::beg);

    if(file.eof()){ // Assert we are not in EOF.
        std::cout << EOF_MESSAGE;
        return;
    }
	
    i = 0;

    while(i < longSize){
		
        if(file.eof()){ // Assert we are not in EOF.
            std::cout << EOF_MESSAGE;
            return;
        }
		
        if(isLE){
            file >> addressOfEntryPoint[i++];
            continue;
        }
		
        file >> addressOfEntryPoint[longSize - 1 - i++];
		
    }
	
    std::cout << "Address of Entry Point for file " << path << " is " << *((unsigned long *) addressOfEntryPoint) << " (0x" << std::hex << *((unsigned long *) addressOfEntryPoint) << ")\n";
    file.close();
	
}

int isLittleEndian(){
	
    int x = 1;
    char *px = (char *) &x;
    return *px;
	
}

void printDir(char *path){
	
    assert(path);
	
    WIN32_FIND_DATA foundFile;
    HANDLE handler = INVALID_HANDLE_VALUE;
	
    char buffer[MAX_PATH];
    unsigned int pathLength = strlen(path);
	
    if(pathLength > MAX_PATH - 3){ // -3 because latter we'll have to concat \\* to dir path (api requirements)...
        std::cout << "Directory path is too long (O.S. limitation)...";
        return;
    }
	
    strncpy(buffer, path, pathLength + 1);
    strncat(buffer, "\\*", 3);
	
    handler = FindFirstFile(buffer, &foundFile);
	
    if(handler == INVALID_HANDLE_VALUE){
        std::cout << "Cannot find " << path << " dir ...";
        return;
    }
	
    do{
		
        if(foundFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
            std::cout << foundFile.cFileName << " <DIR>\n";
        }
		
        else{
            std::cout << foundFile.cFileName << "\n";
        }
		
    }while(FindNextFile(handler, &foundFile) != 0);
	
    FindClose(handler);
	
}

void printPath(){
	
    LPTCH p = GetEnvironmentStrings();
	
	if(p == NULL){
		std::cout << "\nNo environment variable available for this process... try to run in adm mode.";
		return;
	}
	
    while(*p != '\0'){
		
        std::cout << p << "\n\n";
        p += strlen(p) + 1;
		
    }
	
    FreeEnvironmentStrings(p);
}